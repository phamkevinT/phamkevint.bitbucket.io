'use strict'

let grabberButtonIsOn = true
let classButtonIsOn = false
let simpleLineEdgeButtonIsOn = false
let dottedLineEdgeButtonIsOn = false
let diamondLineEdgeButtonIsOn = false
let openLineEdgeButtonIsOn = false
let interfaceButtonIsOn = false

function grabberButtonPressed() {
  grabberButtonIsOn = true
  classButtonIsOn = false
  simpleLineEdgeButtonIsOn = false
  dottedLineEdgeButtonIsOn = false
  diamondLineEdgeButtonIsOn = false
  openLineEdgeButtonIsOn = false
  interfaceButtonIsOn = false

  const grabberButton = document.getElementById('grabberButton')
  grabberButton.style.backgroundColor = 'gray'

  const classButton = document.getElementById('classButton')
  classButton.style.backgroundColor = ''

  const interfaceButton = document.getElementById('interfaceButton')
  interfaceButton.style.backgroundColor = ''

  const simpleLineEdgeButton = document.getElementById('simpleLineEdgeButton')
  simpleLineEdgeButton.style.backgroundColor = ''

  const dottedLineEdgeButton = document.getElementById('dottedLineEdgeButton')
  dottedLineEdgeButton.style.backgroundColor = ''
  
  const diamondLineEdgeButton = document.getElementById('diamondLineEdgeButton')
  diamondLineEdgeButton.style.backgroundColor = ''

  const openLineEdgeButton = document.getElementById('openLineEdgeButton')
  openLineEdgeButton.style.backgroundColor = ''

}
grabberButtonPressed();

function classButtonPressed() {
  classButtonIsOn = true
  grabberButtonIsOn = false
  simpleLineEdgeButtonIsOn = false
  dottedLineEdgeButtonIsOn = false
  diamondLineEdgeButtonIsOn = false
  openLineEdgeButtonIsOn = false
  interfaceButtonIsOn = false

  const classButton = document.getElementById('classButton')
  classButton.style.backgroundColor = 'gray'

  const grabberButton = document.getElementById('grabberButton')
  grabberButton.style.backgroundColor = ''

  const interfaceButton = document.getElementById('interfaceButton')
  interfaceButton.style.backgroundColor = ''

  const simpleLineEdgeButton = document.getElementById('simpleLineEdgeButton')
  simpleLineEdgeButton.style.backgroundColor = ''

  const dottedLineEdgeButton = document.getElementById('dottedLineEdgeButton')
  dottedLineEdgeButton.style.backgroundColor = ''
  
  const diamondLineEdgeButton = document.getElementById('diamondLineEdgeButton')
  diamondLineEdgeButton.style.backgroundColor = ''

  const openLineEdgeButton = document.getElementById('openLineEdgeButton')
  openLineEdgeButton.style.backgroundColor = ''

}

function interfaceButtonPressed() {
  interfaceButtonIsOn = true
  classButtonIsOn = false
  grabberButtonIsOn = false
  simpleLineEdgeButtonIsOn = false
  dottedLineEdgeButtonIsOn = false
  diamondLineEdgeButtonIsOn = false
  openLineEdgeButtonIsOn = false

  const interfaceButton = document.getElementById('interfaceButton')
  interfaceButton.style.backgroundColor = 'gray'

  const classButton = document.getElementById('classButton')
  classButton.style.backgroundColor = ''

  const grabberButton = document.getElementById('grabberButton')
  grabberButton.style.backgroundColor = ''

  const simpleLineEdgeButton = document.getElementById('simpleLineEdgeButton')
  simpleLineEdgeButton.style.backgroundColor = ''

  const dottedLineEdgeButton = document.getElementById('dottedLineEdgeButton')
  dottedLineEdgeButton.style.backgroundColor = ''
  
  const diamondLineEdgeButton = document.getElementById('diamondLineEdgeButton')
  diamondLineEdgeButton.style.backgroundColor = ''

  const openLineEdgeButton = document.getElementById('openLineEdgeButton')
  openLineEdgeButton.style.backgroundColor = ''
}

function simpleLineEdgeButtonPressed() {
  simpleLineEdgeButtonIsOn = true
  grabberButtonIsOn = false
  classButtonIsOn = false
  dottedLineEdgeButtonIsOn = false
  diamondLineEdgeButtonIsOn = false
  openLineEdgeButtonIsOn = false
  interfaceButtonIsOn = false

  const simpleLineEdgeButton = document.getElementById('simpleLineEdgeButton')
  simpleLineEdgeButton.style.backgroundColor = 'gray'

  const grabberButton = document.getElementById('grabberButton')
  grabberButton.style.backgroundColor = ''

  const classButton = document.getElementById('classButton')
  classButton.style.backgroundColor = ''

  const interfaceButton = document.getElementById('interfaceButton')
  interfaceButton.style.backgroundColor = ''

  const dottedLineEdgeButton = document.getElementById('dottedLineEdgeButton')
  dottedLineEdgeButton.style.backgroundColor = ''
  
  const diamondLineEdgeButton = document.getElementById('diamondLineEdgeButton')
  diamondLineEdgeButton.style.backgroundColor = ''

  const openLineEdgeButton = document.getElementById('openLineEdgeButton')
  openLineEdgeButton.style.backgroundColor = ''

}

function dottedLineEdgeButtonPressed() {
  dottedLineEdgeButtonIsOn = true
  grabberButtonIsOn = false
  classButtonIsOn = false
  simpleLineEdgeButtonIsOn = false
  diamondLineEdgeButtonIsOn = false
  openLineEdgeButtonIsOn = false
  interfaceButtonIsOn = false

  const dottedLineEdgeButton = document.getElementById('dottedLineEdgeButton')
  dottedLineEdgeButton.style.backgroundColor = 'gray'
  
  const grabberButton = document.getElementById('grabberButton')
  grabberButton.style.backgroundColor = ''

  const classButton = document.getElementById('classButton')
  classButton.style.backgroundColor = ''

  const interfaceButton = document.getElementById('interfaceButton')
  interfaceButton.style.backgroundColor = ''

  const simpleLineEdgeButton = document.getElementById('simpleLineEdgeButton')
  simpleLineEdgeButton.style.backgroundColor = ''
  
  const diamondLineEdgeButton = document.getElementById('diamondLineEdgeButton')
  diamondLineEdgeButton.style.backgroundColor = ''

  const openLineEdgeButton = document.getElementById('openLineEdgeButton')
  openLineEdgeButton.style.backgroundColor = ''

}


function diamondLineEdgeButtonPressed() {
  dottedLineEdgeButtonIsOn = false
  grabberButtonIsOn = false
  classButtonIsOn = false
  simpleLineEdgeButtonIsOn = false
  diamondLineEdgeButtonIsOn = true
  openLineEdgeButtonIsOn = false
  interfaceButtonIsOn = false

  const diamondLineEdgeButton = document.getElementById('diamondLineEdgeButton')
  diamondLineEdgeButton.style.backgroundColor = 'gray'
  
  const grabberButton = document.getElementById('grabberButton')
  grabberButton.style.backgroundColor = ''

  const classButton = document.getElementById('classButton')
  classButton.style.backgroundColor = ''

  const interfaceButton = document.getElementById('interfaceButton')
  interfaceButton.style.backgroundColor = ''

  const simpleLineEdgeButton = document.getElementById('simpleLineEdgeButton')
  simpleLineEdgeButton.style.backgroundColor = ''
  
  const dottedLineEdgeButton = document.getElementById('dottedLineEdgeButton')
  dottedLineEdgeButton.style.backgroundColor = ''

  const openLineEdgeButton = document.getElementById('openLineEdgeButton')
  openLineEdgeButton.style.backgroundColor = ''

}



function openLineEdgeButtonPressed() {
  dottedLineEdgeButtonIsOn = false
  grabberButtonIsOn = false
  classButtonIsOn = false
  simpleLineEdgeButtonIsOn = false
  diamondLineEdgeButtonIsOn = false
  openLineEdgeButtonIsOn = true
  interfaceButtonIsOn = false
  
  const openLineEdgeButton = document.getElementById('openLineEdgeButton')
  openLineEdgeButton.style.backgroundColor = 'gray'
  
  const grabberButton = document.getElementById('grabberButton')
  grabberButton.style.backgroundColor = ''
  
  const classButton = document.getElementById('classButton')
  classButton.style.backgroundColor = ''
  
  const interfaceButton = document.getElementById('interfaceButton')
  interfaceButton.style.backgroundColor = ''
  
  const simpleLineEdgeButton = document.getElementById('simpleLineEdgeButton')
  simpleLineEdgeButton.style.backgroundColor = ''
  
  const diamondLineEdgeButton = document.getElementById('diamondLineEdgeButton')
  diamondLineEdgeButton.style.backgroundColor = ''
  
  const dottedLineEdgeButton = document.getElementById('dottedLineEdgeButton')
  dottedLineEdgeButton.style.backgroundColor = ''
  
}


function center(rect) {
  return { x: rect.x + rect.width / 2, y: rect.y + rect.height / 2 }
}


function drawGrabber(x, y) {
  const size = 5;
  const panel = document.getElementById('graphpanel')
  const ctx = panel.getContext('2d')
  ctx.fillStyle = 'black'
  ctx.fillRect(x - size / 2 , y - size / 2, size, size);
}

function createClassBox (x, y) {
  let width = 100
  let height = 125
  let className = undefined
  let attributeName = undefined // KP
  let methodName = undefined // KP 

  return {
    getBounds: () => {
      return {
        x: x,
        y: y,
        width: width,
        height: height,
      }
    },
    contains: p => {
      return (x + width / 2 - p.x) ** 2 + (y + height / 2 - p.y) ** 2 <= height ** 2 / 4
    },
    translate: (dx, dy) => {
      x += dx
      y += dy
    },
    setClassName: (newClassName) => {      
      className = newClassName
    },
    setAttributeName: (newAttributeName) => {      // KP
      attributeName = newAttributeName
    },
    setMethodName: (newMethodName) => {      // KP
      methodName = newMethodName
    },

    draw: () => {
      const panel = document.getElementById('graphpanel')
      const ctx = panel.getContext('2d')
      const centerX = x + width / 2
      const centerY = y + height / 2

      // KP Needed to divde class box into 3 sections
      const firstThirdY = y + height / 3
      const secondThirdY = y + 2*(height / 3)

      // KP Divides Class Box
      ctx.beginPath;
      ctx.moveTo(x, firstThirdY);
      ctx.lineTo(x+width, firstThirdY);
      ctx.stroke();
      // KP Divides Class Box
      ctx.beginPath;
      ctx.moveTo(x, secondThirdY);
      ctx.lineTo(x+width, secondThirdY);
      ctx.stroke();
      // KP Class Name Label
      ctx.fillStyle = "red"
      ctx.fillText("Class Name", x+2, y+10);
      ctx.fillStyle = "black";
      // KP Attribute Name Label
      ctx.fillStyle = "red"
      ctx.fillText("Attributes", x+2, firstThirdY+10);
      ctx.fillStyle = "black";
      // KP Methods Name Label
      ctx.fillStyle = "red"
      ctx.fillText("Methods()", x+2, secondThirdY+10);
      ctx.fillStyle = "black";

      ctx.rect(x, y, width, height)
      ctx.setLineDash([])
      if (className !== undefined ) {
        ctx.fillText(className, x+10, y+20)
      }
      if (attributeName !== undefined ) {
        ctx.fillText(attributeName, x+10, firstThirdY+20) 
      }
      if (methodName !== undefined ) {
        ctx.fillText(methodName, x+10, secondThirdY+20) 
      }
      ctx.stroke()
    }
    
  }
}

function createInterfaceBox(x, y) {
  let width = 100
  let height = 125
  let interfaceName = undefined
  let interfaceAttributes = undefined // WIP
  let interfaceMethods = undefined


  return {
    getBounds: () => {
      return {
        x: x,
        y: y,
        width: width,
        height: height,
      }
    },
    contains: p => {
      return (x + width / 2 - p.x) ** 2 + (y + height / 2 - p.y) ** 2 <= height ** 2 / 4
    },
    translate: (dx, dy) => {
      x += dx
      y += dy
    },
    setName: (newInterfaceName) => {      
      interfaceName = newInterfaceName
    },
    setClassName: (newClassName) => {      
      interfaceName = newClassName
    },
    setAttributeName: (newAttributeName) => {      // KP
      interfaceAttributes = newAttributeName
    },
    setMethodName: (newMethodName) => {      // KP
      interfaceMethods = newMethodName
    },

    draw: () => {
      const panel = document.getElementById('graphpanel')
      const ctx = panel.getContext('2d')
      const centerX = (x + width / 2) - 28
      const centerY = (y + height / 2)

      // KP Needed to divde class box into 3 sections
      const firstThirdY = y + height / 3                // WIP
      const secondThirdY = y + 2*(height / 3)           // WIP

      // KP Divides Interface Box
      ctx.beginPath;
      ctx.style
      ctx.moveTo(x, firstThirdY);
      ctx.lineTo(x+width, firstThirdY);
      ctx.stroke();

      ctx.rect(x, y, width, height)
      ctx.setLineDash([])
      // KP Attribute Name Label
      ctx.fillStyle = "blue"
      ctx.fillText('<<interface>>', x+10, y+10)
      ctx.fillStyle = "black"

      if (interfaceName !== undefined) {
        ctx.fillText(interfaceName, x+10, y+25)
      }
      if (interfaceAttributes !== undefined ) {
        ctx.fillText(interfaceAttributes, x+10, firstThirdY+20) 
      }
      if (interfaceMethods !== undefined ) {
        ctx.fillText(interfaceMethods, x+10, secondThirdY+20) 
      }
      ctx.stroke()
    }

  }
}


function createSimpleLineEdge() {
  let start = undefined
  let end = undefined
  return {
    connect: (s, e) => {
      start = s
      end = e
    },
    draw: () => {
      const canvas = document.getElementById('graphpanel')
      const ctx = canvas.getContext('2d')
      ctx.beginPath()
      const str = start.getBounds()
      const en = end.getBounds()
      const sx = str.x
      const sy = str.y
      const ex = en.x
      const ey = en.y
            if(sy >= ey){
          if(sx > ex + en.width){
            // left side
            ctx.moveTo(sx, sy + str.height/2)
            // middle liners
            ctx.lineTo((sx + ex + en.width)/2, sy + str.height/2)
            ctx.lineTo((sx + ex + en.width)/2, ey + en.height/2)
            // right side
            ctx.lineTo(ex + en.width, ey + en.height/2)
            ctx.setLineDash([])
            ctx.stroke()
            ctx.beginPath()
            ctx.moveTo(ex + en.width, ey + en.height/2)
      		ctx.lineTo(ex + en.width + 10, ey + en.height/2 + 10)
      		ctx.moveTo(ex + en.width, ey + en.height/2)
      		ctx.lineTo(ex + en.width + 10, ey + en.height/2 - 10)
      		ctx.closePath()
      		ctx.setLineDash([])
      		ctx.stroke()
          }
          else if(ex > sx + str.width){
            // right side
            ctx.moveTo(sx + str.width, sy + str.height/2)
            // middle liners
            ctx.lineTo((sx + str.width + ex)/2, sy + str.height/2)
            ctx.lineTo((sx + str.width + ex)/2, ey + en.height/2)
            // left side
            ctx.lineTo(ex, ey + en.height/2)
            ctx.setLineDash([])
            ctx.stroke()
            ctx.beginPath()
            ctx.moveTo(ex, ey + en.height/2)
      		ctx.lineTo(ex - 10, ey + en.height/2 + 10)
      		ctx.moveTo(ex, ey + en.height/2)
      		ctx.lineTo(ex - 10, ey + en.height/2 - 10)
      		ctx.closePath()
      		ctx.setLineDash([])
      		ctx.stroke()
          }
          else{
            // top
            ctx.moveTo(sx + str.width/2, sy)
            // middle liners
            ctx.lineTo(sx + str.width/2, (sy + ey + en.height)/2)
            ctx.lineTo(ex + en.width/2, (sy + ey + en.height)/2)
            // bottom
            ctx.lineTo(ex + en.width/2, ey + en.height)
            ctx.setLineDash([])
            ctx.stroke()
            ctx.beginPath()
            ctx.moveTo(ex + en.width/2, ey + en.height)
      		ctx.lineTo(ex + en.width/2 + 10, ey + en.height + 10)
      		ctx.moveTo(ex + en.width/2, ey + en.height)
      		ctx.lineTo(ex + en.width/2 - 10, ey + en.height + 10)
      		ctx.closePath()
      		ctx.setLineDash([])
      		ctx.stroke()
          }
        }
        else{
          if(sx > ex + en.width){
            // left side
            ctx.moveTo(sx, sy + str.height/2)
            // middle liners
            ctx.lineTo((sx + ex + en.width)/2, sy + str.height/2)
            ctx.lineTo((sx + ex + en.width)/2, ey + en.height/2)
            // right side
            ctx.lineTo(ex + en.width, ey + en.height/2)
            ctx.setLineDash([])
            ctx.stroke()
            ctx.beginPath()
            ctx.moveTo(ex + en.width, ey + en.height/2)
      		ctx.lineTo(ex + en.width + 10, ey + en.height/2 + 10)
      		ctx.moveTo(ex + en.width, ey + en.height/2)
      		ctx.lineTo(ex + en.width + 10, ey + en.height/2 - 10)
      		ctx.closePath()
      		ctx.setLineDash([])
      		ctx.stroke()
          }
          else if(ex > sx + str.width){
            // right side
            ctx.moveTo(sx + str.width, sy + str.height/2)
            // middle liners
            ctx.lineTo((sx + str.width + ex)/2, sy + str.height/2)
            ctx.lineTo((sx + str.width + ex)/2, ey + en.height/2)
            // left side
            ctx.lineTo(ex, ey + en.height/2)
            ctx.setLineDash([])
            ctx.stroke()
            ctx.beginPath()
            ctx.moveTo(ex, ey + en.height/2)
      		ctx.lineTo(ex - 10, ey + en.height/2 + 10)
      		ctx.moveTo(ex, ey + en.height/2)
      		ctx.lineTo(ex - 10, ey + en.height/2 - 10)
      		ctx.closePath()
      		ctx.setLineDash([])
      		ctx.stroke()
          }
          else{
            // bottom
            ctx.moveTo(sx + str.width/2, sy + str.height)
            // middle liners
            ctx.lineTo(sx + str.width/2, (sy + ey + en.height)/2)
            ctx.lineTo(ex + en.width/2, (sy + ey + en.height)/2)
            // top
            ctx.lineTo(ex + en.width/2, ey)
            ctx.setLineDash([])
            ctx.stroke()
            ctx.beginPath()
            ctx.moveTo(ex + en.width/2, ey)
      		ctx.lineTo(ex + en.width/2 + 10, ey - 10)
      		ctx.moveTo(ex + en.width/2, ey)
      		ctx.lineTo(ex + en.width/2 - 10, ey - 10)
      		ctx.closePath()
      		ctx.setLineDash([])
      		ctx.stroke()
          }
        }   
    }
  }
}

function createArrowLineEdge() {
  let start = undefined
  let end = undefined
  return {
    connect: (s, e) => {
      start = s
      end = e
    },
    draw: () => {
      const canvas = document.getElementById('graphpanel')
      const ctx = canvas.getContext('2d')
      const p = center(start.getBounds()) 
      const q = center(end.getBounds()) 
      
      let angle = Math.atan2(q.y - p.y, q.x - p.x)
      let headlength = 10

      // Begin center line of the arrow      
      ctx.beginPath()
      ctx.moveTo(p.x, p.y)
      ctx.lineTo(q.x, q.y)
      ctx.stroke()

      // Begin the head
      ctx.beginPath()
      ctx.moveTo(p.x, p.y)
      ctx.lineto(p.x - headlength * Math.cos(angle - Math.PI / 7), q.y - headlength*Math.sin(angle - Math.PI / 7))

      ctx.lineto(p.x - headlength * Math.cos(angle + Math.PI / 7), q.y - headlength*Math.sin(angle + Math.PI / 7))
      
      ctx.lineTo(q.x, q.y)
      ctx.lineto(p.x - headlength * Math.cos(angle - Math.PI / 7), q.y - headlength*Math.sin(angle - Math.PI / 7))

      ctx.stroke()
      
    }
  }
}

function createDottedLineEdge() {
let start = undefined
  let end = undefined
  return {
    connect: (s, e) => {
      start = s
      end = e
    },
    draw: () => {
      const canvas = document.getElementById('graphpanel')
      const ctx = canvas.getContext('2d')
      ctx.beginPath()
          const str = start.getBounds()
      const en = end.getBounds()
      const sx = str.x
      const sy = str.y
      const ex = en.x
      const ey = en.y
   if(sy >= ey){
          if(sx > ex + en.width){
            // left side
            ctx.moveTo(sx, sy + str.height/2)
            ctx.lineTo((sx + ex + en.width)/2, sy + str.height/2)
            ctx.lineTo((sx + ex + en.width)/2, ey + en.height/2)
            // right side
            ctx.lineTo(ex + en.width, ey + en.height/2)
            ctx.setLineDash([2, 4])
            ctx.stroke()
            ctx.beginPath()
            ctx.moveTo(ex + en.width, ey + en.height/2)
      		ctx.lineTo(ex + en.width + 10, ey + en.height/2 + 10)
      		ctx.moveTo(ex + en.width, ey + en.height/2)
      		ctx.lineTo(ex + en.width + 10, ey + en.height/2 - 10)
      		ctx.closePath()
      		ctx.setLineDash([])
      		ctx.stroke()
          }
          else if(ex > sx + str.width){
            // right side
            ctx.moveTo(sx + str.width, sy + str.height/2)
            // middle liners
            ctx.lineTo((sx + str.width + ex)/2, sy + str.height/2)
            ctx.lineTo((sx + str.width + ex)/2, ey + en.height/2)
            // left side
            ctx.lineTo(ex, ey + en.height/2)
            ctx.setLineDash([2, 4])
            ctx.stroke()
            ctx.beginPath()
           ctx.moveTo(ex, ey + en.height/2)
      		ctx.lineTo(ex - 10, ey + en.height/2 + 10)
      		ctx.moveTo(ex, ey + en.height/2)
      		ctx.lineTo(ex - 10, ey + en.height/2 - 10)
      		ctx.closePath()
      		ctx.setLineDash([])
      		ctx.stroke()
          }
          else{
            // top
            ctx.moveTo(sx + str.width/2, sy)
            // middle liners
            ctx.lineTo(sx + str.width/2, (sy + ey + en.height)/2)
            ctx.lineTo(ex + en.width/2, (sy + ey + en.height)/2)
            // bottom
            ctx.lineTo(ex + en.width/2, ey + en.height)
            ctx.setLineDash([2, 4])
            ctx.stroke()
            ctx.beginPath()
            ctx.moveTo(ex + en.width/2, ey + en.height)
      		ctx.lineTo(ex + en.width/2 + 10, ey + en.height + 10)
      		ctx.moveTo(ex + en.width/2, ey + en.height)
      		ctx.lineTo(ex + en.width/2 - 10, ey + en.height + 10)
      		ctx.closePath()
      		ctx.setLineDash([])
      		ctx.stroke()
          }
        }
        else{
          if(sx > ex + en.width){
            // left side
            ctx.moveTo(sx, sy + str.height/2)
            ctx.lineTo((sx + ex + en.width)/2, sy + str.height/2)
            ctx.lineTo((sx + ex + en.width)/2, ey + en.height/2)
            // right side
            ctx.lineTo(ex + en.width, ey + en.height/2)
            ctx.setLineDash([2, 4])
            ctx.stroke()
            ctx.beginPath()
            ctx.moveTo(ex + en.width, ey + en.height/2)
      		ctx.lineTo(ex + en.width + 10, ey + en.height/2 + 10)
      		ctx.moveTo(ex + en.width, ey + en.height/2)
      		ctx.lineTo(ex + en.width + 10, ey + en.height/2 - 10)
      		ctx.closePath()
      		ctx.setLineDash([])
      		ctx.stroke()
          }
          else if(ex > sx + str.width){
            // right side
            ctx.moveTo(sx + str.width, sy + str.height/2)
            // middle liners
            ctx.lineTo((sx + str.width + ex)/2, sy + str.height/2)
            ctx.lineTo((sx + str.width + ex)/2, ey + en.height/2)
            // left side
            ctx.lineTo(ex, ey + en.height/2)
            ctx.setLineDash([2, 4])
            ctx.stroke()
            ctx.beginPath()
            ctx.moveTo(ex, ey + en.height/2)
      		ctx.lineTo(ex - 10, ey + en.height/2 + 10)
      		ctx.moveTo(ex, ey + en.height/2)
      		ctx.lineTo(ex - 10, ey + en.height/2 - 10)
      		ctx.closePath()
      		ctx.setLineDash([])
      		ctx.stroke()
          }
          else{
            // bottom
            ctx.moveTo(sx + str.width/2, sy + str.height)
            // middle liners
            ctx.lineTo(sx + str.width/2, (sy + ey + en.height)/2)
            ctx.lineTo(ex + en.width/2, (sy + ey + en.height)/2)
            // top
            ctx.lineTo(ex + en.width/2, ey)
            ctx.setLineDash([2, 4])
            ctx.stroke()
            ctx.beginPath()
            ctx.moveTo(ex + en.width/2, ey)
      		ctx.lineTo(ex + en.width/2 + 10, ey - 10)
      		ctx.moveTo(ex + en.width/2, ey)
      		ctx.lineTo(ex + en.width/2 - 10, ey - 10)
      		ctx.closePath()
      		ctx.setLineDash([])
      		ctx.stroke()
          }
        }
    }
  }
}


function createDiamondLineEdge(){
	  let start = undefined
  let end = undefined
  return {
    connect: (s, e) => {
      start = s
      end = e
    },
    draw: () => {
      const canvas = document.getElementById('graphpanel')
      const ctx = canvas.getContext('2d')
      ctx.beginPath()
      const str = start.getBounds()
      const en = end.getBounds()
      const sx = str.x
      const sy = str.y
      const ex = en.x
      const ey = en.y
             if(sy >= ey){
            if(sx > ex + en.width){
                // left side                
                ctx.moveTo(sx - 20,  sy + str.height/2)
                ctx.lineTo((sx + ex + en.width)/2, sy + str.height/2)
                ctx.lineTo((sx + ex + en.width)/2, ey + en.height/2)
                // right side
                ctx.lineTo(ex + en.width, ey + en.height/2)
                ctx.setLineDash([])
                ctx.stroke()
                ctx.closePath()
                ctx.beginPath()
                ctx.moveTo(sx, sy + str.height/2)
                ctx.lineTo(sx - 10,  sy + str.height/2 + 10)
                ctx.lineTo(sx - 20,  sy + str.height/2)
                ctx.lineTo(sx - 10,  sy + str.height/2 - 10)
                ctx.closePath()
                ctx.setLineDash([])
                ctx.stroke()
            }
            else if(ex > sx + str.width){
                // right side                
                ctx.moveTo(sx + str.width + 20, sy + str.height/2)
                // middle liners
                ctx.lineTo((sx + str.width + ex)/2, sy + str.height/2)
                ctx.lineTo((sx + str.width + ex)/2, ey + en.height/2)
                // left side
                ctx.lineTo(ex, ey + en.height/2)
                ctx.setLineDash([])
                ctx.stroke()
                ctx.closePath()
                ctx.beginPath()
                ctx.moveTo(sx + str.width, sy +str.height/2)
                ctx.lineTo(sx + str.width + 10, sy + str.height/2 + 10)
                ctx.lineTo(sx + str.width + 20, sy + str.height/2)
                ctx.lineTo(sx + str.width + 10, sy + str.height/2 - 10)
                ctx.closePath()
                ctx.setLineDash([])
                ctx.stroke()
            }
            else{
                // top                
                ctx.moveTo(sx + str.width/2, sy - 20)
                // middle liners
                ctx.lineTo(sx + str.width/2, (sy + ey + en.height)/2)
                ctx.lineTo(ex + en.width/2, (sy + ey + en.height)/2)
                // bottom
                ctx.lineTo(ex + en.width/2, ey + en.height)
                ctx.setLineDash([])
                ctx.stroke()
                ctx.closePath()
                ctx.beginPath() 
                ctx.moveTo(sx + str.width/2, sy)
                ctx.lineTo(sx + str.width/2 - 10, sy- 10)
                ctx.lineTo(sx + str.width/2, sy - 20)
                ctx.lineTo(sx + str.width/2 + 10, sy - 10)
                ctx.closePath()
                ctx.setLineDash([])
                ctx.stroke()
            }
        }
        else{
            if(sx > ex + en.width){
                // left side
                ctx.moveTo(sx - 20,  sy + str.height/2)
                ctx.lineTo((sx + ex + en.width)/2, sy + str.height/2)
                ctx.lineTo((sx + ex + en.width)/2, ey + en.height/2)
                // right side
                ctx.lineTo(ex + en.width, ey + en.height/2)
                ctx.setLineDash([])
                ctx.stroke()
                ctx.closePath()
                ctx.beginPath()
                ctx.moveTo(sx, sy + str.height/2)
                ctx.lineTo(sx - 10,  sy + str.height/2 + 10)
                ctx.lineTo(sx - 20,  sy + str.height/2)
                ctx.lineTo(sx - 10,  sy + str.height/2 - 10)
                ctx.closePath()
                ctx.setLineDash([])
                ctx.stroke()
            }
            else if(ex > sx + str.width){
                // right side
                ctx.moveTo(sx + str.width + 20, sy + str.height/2)
                // middle liners
                ctx.lineTo((sx + str.width + ex)/2, sy + str.height/2)
                ctx.lineTo((sx + str.width + ex)/2, ey + en.height/2)
                // left side
                ctx.lineTo(ex, ey + en.height/2)
                ctx.setLineDash([])
                ctx.stroke()
                ctx.closePath()
                ctx.beginPath()
                ctx.moveTo(sx + str.width, sy +str.height/2)
                ctx.lineTo(sx + str.width + 10, sy + str.height/2 + 10)
                ctx.lineTo(sx + str.width + 20, sy + str.height/2)
                ctx.lineTo(sx + str.width + 10, sy + str.height/2 - 10)
                ctx.closePath()
                ctx.setLineDash([])
                ctx.stroke()
            }
            else{
                // bottom
                ctx.moveTo(sx + str.width/2, sy + str.height + 20)
                // middle liners
                ctx.lineTo(sx + str.width/2, (sy + ey + en.height)/2)
                ctx.lineTo(ex + en.width/2, (sy + ey + en.height)/2)
                // top
                ctx.lineTo(ex + en.width/2, ey)
                ctx.setLineDash([])
                ctx.stroke()
                ctx.closePath()
                ctx.beginPath()
                ctx.moveTo(sx + str.width/2, sy + str.height)
                ctx.lineTo(sx + str.width/2 + 10, sy + str.height + 10)
                ctx.lineTo(sx + str.width/2, sy + str.height + 20)
                ctx.lineTo(sx + str.width/2 - 10, sy + str.height + 10)
                ctx.closePath()
                ctx.setLineDash([])
                ctx.stroke()
            }
        }
    }
  }
}
function createOpenTriangleLineEdge(){
	  let start = undefined
  let end = undefined
  return {
    connect: (s, e) => {
      start = s
      end = e
    },
    draw: () => {
      const canvas = document.getElementById('graphpanel')
      const ctx = canvas.getContext('2d')
      ctx.beginPath()
      const str = start.getBounds()
      const en = end.getBounds()
      const sx = str.x
      const sy = str.y
      const ex = en.x
      const ey = en.y
if(sy >= ey){
            if(sx > ex + en.width){
                // left side
                ctx.moveTo(sx, sy + str.height/2)
                ctx.lineTo((sx + ex + en.width)/2, sy + str.height/2)
                ctx.lineTo((sx + ex + en.width)/2, ey + en.height/2)
                // right side
                ctx.lineTo(ex + en.width + 10, ey + en.height/2)
                ctx.setLineDash([])
                ctx.stroke()
                ctx.closePath()
                ctx.beginPath()
                ctx.moveTo(ex + en.width, ey + en.height/2)
                ctx.lineTo(ex + en.width + 10, ey + en.height/2 + 10)
                ctx.moveTo(ex + en.width, ey + en.height/2)
                ctx.lineTo(ex + en.width + 10, ey + en.height/2 - 10)
                ctx.lineTo(ex + en.width + 10, ey + en.height/2 + 10)
                ctx.closePath()
                ctx.setLineDash([])
                ctx.stroke()
            }
            else if(ex > sx + str.width){
                // right side
                ctx.moveTo(sx + str.width, sy + str.height/2)
                // middle liners
                ctx.lineTo((sx + str.width + ex)/2, sy + str.height/2)
                ctx.lineTo((sx + str.width + ex)/2, ey + en.height/2)
                // left side
                ctx.lineTo(ex - 10, ey + en.height/2)
                ctx.setLineDash([])
                ctx.stroke()
                ctx.closePath()
                ctx.beginPath()
                ctx.moveTo(ex, ey + en.height/2)
                ctx.lineTo(ex - 10, ey + en.height/2 + 10)
                ctx.moveTo(ex, ey + en.height/2)
                ctx.lineTo(ex - 10, ey + en.height/2 - 10)
                ctx.lineTo(ex - 10, ey + en.height/2 + 10)
                ctx.closePath()
                ctx.setLineDash([])
                ctx.stroke()
            }
            else{
                // top
                ctx.moveTo(sx + str.width/2, sy)
                // middle liners
                ctx.lineTo(sx + str.width/2, (sy + ey + en.height)/2)
                ctx.lineTo(ex + en.width/2, (sy + ey + en.height)/2)
                // bottom
                ctx.lineTo(ex + en.width/2, ey + en.height + 10)
                ctx.setLineDash([])
                ctx.stroke()
                ctx.closePath()
                ctx.beginPath()
                ctx.moveTo(ex + en.width/2, ey + en.height)
                ctx.lineTo(ex + en.width/2 + 10, ey + en.height + 10)
                ctx.moveTo(ex + en.width/2, ey + en.height)
                ctx.lineTo(ex + en.width/2 - 10, ey + en.height + 10)
                ctx.lineTo(ex + en.width/2 + 10, ey + en.height + 10)
                ctx.closePath()
                ctx.setLineDash([])
                ctx.stroke()
            }
        }
        else{
            if(sx > ex + en.width){
                // left side
                ctx.moveTo(sx, sy + str.height/2)
                ctx.lineTo((sx + ex + en.width)/2, sy + str.height/2)
                ctx.lineTo((sx + ex + en.width)/2, ey + en.height/2)
                // right side
                ctx.lineTo(ex + en.width + 10, ey + en.height/2)
                ctx.setLineDash([])
                ctx.stroke()
                ctx.closePath()
                ctx.beginPath()
                ctx.moveTo(ex + en.width, ey + en.height/2)
                ctx.lineTo(ex + en.width + 10, ey + en.height/2 + 10)
                //ctx.moveTo(ex + en.width, ey + en.height/2)
                ctx.lineTo(ex + en.width + 10, ey + en.height/2 - 10)
                //ctx.lineTo(ex + en.width + 10, ey + en.height/2 + 10)
                ctx.closePath()
                ctx.setLineDash([])
                ctx.stroke()
            }
            else if(ex > sx + str.width){
                // right side
                ctx.moveTo(sx + str.width, sy + str.height/2)
                // middle liners
                ctx.lineTo((sx + str.width + ex)/2, sy + str.height/2)
                ctx.lineTo((sx + str.width + ex)/2, ey + en.height/2)
                // left side
                ctx.lineTo(ex - 10, ey + en.height/2)
                ctx.setLineDash([])
                ctx.stroke()
                ctx.closePath()
                ctx.beginPath()
                ctx.moveTo(ex, ey + en.height/2)
                ctx.lineTo(ex - 10, ey + en.height/2 + 10)
                ctx.moveTo(ex, ey + en.height/2)
                ctx.lineTo(ex - 10, ey + en.height/2 - 10)
                ctx.lineTo(ex - 10, ey + en.height/2 + 10)
                ctx.closePath()
                ctx.setLineDash([])
                ctx.stroke()
            }
            else{
                // bottom
                ctx.moveTo(sx + str.width/2, sy + str.height)
                // middle liners
                ctx.lineTo(sx + str.width/2, (sy + ey + en.height)/2)
                ctx.lineTo(ex + en.width/2, (sy + ey + en.height)/2)
                // top
                ctx.lineTo(ex + en.width/2, ey - 10)
                ctx.setLineDash([])
                ctx.stroke()
                ctx.closePath()
                ctx.beginPath()
                ctx.moveTo(ex + en.width/2, ey)
                ctx.lineTo(ex + en.width/2 + 10, ey - 10)
                ctx.moveTo(ex + en.width/2, ey)
                ctx.lineTo(ex + en.width/2 - 10, ey - 10)
                ctx.lineTo(ex + en.width/2 + 10, ey - 10)
                ctx.closePath()
                ctx.setLineDash([])
                ctx.stroke()
            }
        }
    }
  }
}

function createGraph() {
  let nodes = []
  let edges = []

  return {
    add: (n) => {
      nodes.push(n)
    },
    findNode: (p) => {
      for (let i = nodes.length - 1; i >= 0; i--) {
        const n = nodes[i]
        if (n.contains(p)) return n
      }
      return undefined
    },
    draw: () => {
      for (const n of nodes) {
        n.draw()
      }
      // Draws the lines that connect circles
      for (const e of edges) {
        e.draw()
      }
    },
    connect: (e, p1, p2, g) => {
      const n1 = g.findNode(p1)
      const n2 = g.findNode(p2)
      if (n1 !== undefined && n2 !== undefined) {
        e.connect(n1, n2)
        edges.push(e)
        return true
      }
      return false
    }
  }
}

let graph = undefined;
let nodeSelectedToEditProperties = undefined
document.addEventListener('DOMContentLoaded', function () {
  graph = createGraph()
  
  const panel = document.getElementById('graphpanel')
  let selected = undefined
  let dragStartPoint = undefined
  let dragStartBounds = undefined

  function repaint() {
   // const canvas = document.getElementById('graphpanel')
    const ctx = panel.getContext('2d')
    //ctx.clearRect(0, 0, panel.width, panel.height)
    //ctx.clearRect(0, 0, canvas.width, canvas.height)
    ctx.clearRect(0, 0, 1000, 500)
    //panel.innnerHTML = ''
    graph.draw()

    if (selected !== undefined) {
      const bounds = selected.getBounds()
      drawGrabber(bounds.x, bounds.y)
      drawGrabber(bounds.x + bounds.width, bounds.y)
      drawGrabber(bounds.x, bounds.y + bounds.height)      
      drawGrabber(bounds.x + bounds.width, bounds.y + bounds.height)
    }    
  }

  function mouseLocation(event) {
    var rect = panel.getBoundingClientRect();
    return {
      x: event.clientX - rect.left,
      y: event.clientY - rect.top,
    }
  }
  
  panel.addEventListener('mousedown', event => {
    let mousePoint = mouseLocation(event)
    selected = graph.findNode(mousePoint)
    let newNode = undefined;

    // If a node was clicked on
    if (selected !== undefined) {
      dragStartPoint = mousePoint
      dragStartBounds = selected.getBounds()
    } 
    // If the canvas was clicked on and the class button is active
    else if (selected === undefined && classButtonIsOn) {
      newNode = createClassBox(mousePoint.x, mousePoint.y)

      // Check to make sure the new node is within the canvas. If not, increase canvas size
      const bounds = newNode.getBounds()
      const canvas = document.getElementById('graphpanel')
      const isWithinCanvas = ((bounds.x + bounds.width) < canvas.width) && ((bounds.y + bounds.height) < canvas.height)
      
      // Resize if new node is not within canvas
      if (!isWithinCanvas) {     
        let newHeight = canvas.height + (canvas.height/2) 
        let newWidth = canvas.width + (canvas.width/2)

        canvas.height = newHeight
        canvas.width = newWidth
      }
      repaint()
      graph.add(newNode)
    }

    else if (selected === undefined && interfaceButtonIsOn) {
      newNode = createInterfaceBox(mousePoint.x, mousePoint.y)

      const bounds = newNode.getBounds()
      const canvas = document.getElementById('graphpanel')
      const isWithinCanvas = ((bounds.x + bounds.width) < canvas.width) && ((bounds.y + bounds.height) < canvas.height)

      if (!isWithinCanvas) {
        let newHeight =  canvas.height + (canvas.height/2) 
        let newWidth = canvas.width + (canvas.width/2)

        canvas.height = newHeight
        canvas.width = newWidth
      }
      repaint()
      graph.add(newNode)
    }

    repaint()
  })

  panel.addEventListener('mousemove', event => {
    if (dragStartPoint === undefined) return
    let mousePoint = mouseLocation(event)
    if (selected !== undefined) {
      const bounds = selected.getBounds();
      
      if (grabberButtonIsOn || classButtonIsOn) {
        selected.translate(
          dragStartBounds.x - bounds.x 
            + mousePoint.x - dragStartPoint.x,
          dragStartBounds.y - bounds.y 
            + mousePoint.y - dragStartPoint.y);
        repaint()
      }
    }
  })
  
  panel.addEventListener('mouseup', event => {
    if (simpleLineEdgeButtonIsOn || dottedLineEdgeButtonIsOn || diamondLineEdgeButtonIsOn || openLineEdgeButtonIsOn) {
      let mousePoint = mouseLocation(event)
      let selected = graph.findNode(mousePoint)
      
      if (selected !== undefined && dottedLineEdgeButtonIsOn) {        
        const newDottedLineEdge = createDottedLineEdge()
        graph.connect(newDottedLineEdge, dragStartPoint, mousePoint, graph)
      } 
      else if (selected !== undefined && simpleLineEdgeButtonIsOn) {
        const newSimpleLineEdge = createSimpleLineEdge()
        graph.connect(newSimpleLineEdge, dragStartPoint, mousePoint, graph)
      }
      else if (selected !== undefined && diamondLineEdgeButtonIsOn) {
        const newDiamondLineEdge = createDiamondLineEdge()
        graph.connect(newDiamondLineEdge, dragStartPoint, mousePoint, graph)
      }
      else if (selected !== undefined && openLineEdgeButtonIsOn) {
        const newOpenLineEdge = createOpenTriangleLineEdge()
        graph.connect(newOpenLineEdge, dragStartPoint, mousePoint, graph)
      }
      
      repaint()
    }
    
    dragStartPoint = undefined
    dragStartBounds = undefined
  })

  const modal = document.getElementById('modalbox')
  const span = document.getElementsByClassName("close")[0]
  panel.addEventListener('dblclick', event => {

    if (grabberButtonIsOn) {

      // Grab the location of where the mouse was double clicked
      let mousePoint = mouseLocation(event)
      nodeSelectedToEditProperties = graph.findNode(mousePoint)

      // If the double click was on a node
      if (nodeSelectedToEditProperties !== undefined) {

        // Clear out any previous text
        document.getElementById('classBox').value = ""
        document.getElementById('attributeBox').value = ""
        document.getElementById('methodBox').value = ""

        // Open the modal pop up text box
        modal.style.display = "block"
      }
    }
  })

  span.onclick = function () {
    modal.style.display = "none"
  }

  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none"
    }
  }

  const submitButton = document.getElementById('submitTextButton')
  submitButton.onclick = function () {
    // Grab the text inputted
    const classText = document.getElementById('classBox').value
    const attributeText = document.getElementById('attributeBox').value // KP
    const methodText = document.getElementById('methodBox').value // KP
    const modal = document.getElementById('modalbox')

    // The following 3 if-functions:
    // Set text if its not empty at the time of pressing the 'submit' button
    if (classText !== "") {
      nodeSelectedToEditProperties.setClassName(classText)
      modal.style.display = "none"

      // KP Clear the text before exiting 
      document.getElementById('classBox').value = ""
      repaint()
    }

    if (attributeText !== "") {  
      nodeSelectedToEditProperties.setAttributeName(attributeText) 
      modal.style.display = "none"

      // KP Clear the text before exiting
      document.getElementById('attributeBox').value = "" 
      repaint()
    }
    if (methodText !== "") {
      nodeSelectedToEditProperties.setMethodName(methodText)  
      modal.style.display = "none"

      // KP Clear the text before exiting 
      document.getElementById('methodBox').value = "" 
      repaint()
    }
  }
})